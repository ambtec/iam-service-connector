# IAM Service Connector

Is the customization endpoint for `` iam-service ``. After an successfull login to `` iam-service `` frontend an redirect to `` IAM Service Connector `` will happen. On request a GET parameter `` Code `` is expected. If such an GET parameter `` code `` is received this code will be passed on to the requested recipient via PostMessage.

A valid recipient of that PostMessage is set via ENV `` PM_CLIENT `` as address. This make sure only a valid recipient receives the code for a one time authorization.

### Configuration

Require Keycloak Redirect URL to be setup with the connector address.
![](assets/Keycloak-Redirect-URL.png?raw=true)

#### ENV's

- IAM_ADDRESS: A valid address to receive a code request from. `` http://{KEYCLOAK_ADDRESS}/auth/realms/{KEYCLOAK_REALM}/protocol/openid-connect/auth?client_id=account&response_type=code&redirect_uri={KEYCLOAK_REDIRECT_URL} ``
- PM_CLIENT: A valid recipient to receive the code. `` http://{RECIPIENT_ADDRESS} ``


