<?php

if (!isset($_GET['code'])) {
  header('Location: ' . $_ENV['IAM_ADDRESS']);
  exit;
}

?>
<html>
<head>
  <title>Keycloak: iam connector</title>
  <link rel="shortcut icon" href="src/favicon.png" />
</head>
<body>

<script>
(function(address) {
  var data = {
    component: 'keycloak',
    body: {
      key: 'auth_code',
      value: '<?php echo $_GET['code']; ?>'
    }
  };
  parent.postMessage(data, address);
})('<?php echo $_ENV['PM_CLIENT']; ?>');
</script>

</body>
</html>